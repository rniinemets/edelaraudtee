package eu.niinemets.edelaraudtee;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class DisplayMessageActivity extends Activity {
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Show the Up button in the action bar.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
		
		setContentView(R.layout.activity_display_message);
		
		// Get the submitted message
		Intent intent 		= getIntent();
		int direction		= intent.getIntExtra(MainActivity.USER_DIRECTION, 0);
		String start_point	= intent.getStringExtra(MainActivity.USER_START_POINT);
		String finish_point	= intent.getStringExtra(MainActivity.USER_FINISH_POINT);
		
		// Create container
		LinearLayout tabel	= (LinearLayout) findViewById(R.id.responseView);
		
		TextView rowText	= new TextView(this);
		rowText.setTextSize(20);
		rowText.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		rowText.setPadding(0, 5, 0, 5);
		
		// Search position index of starting and finishing points
		String[][] timetable= TimeTables.getTimetable(direction, start_point, finish_point);
		int start_pointer	= TimeTables.user_start_point;
		int finish_pointer	= TimeTables.user_finish_point;
		
		// If this is a valid stop (both start and finishing stops)
		if(start_pointer >= 0 && finish_pointer >= 0) {
			rowText.setTextSize(40);
			rowText.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			rowText.setText(start_point + " - " + finish_point);
			
			tabel.addView(rowText);

			// Create a new line of LinearLayout
			LinearLayout newRow	= new LinearLayout(this);
			newRow.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			// Create TextView for table header
			TextView startHeader	= new TextView(this);
			startHeader.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
			startHeader.setTextSize(35);
			startHeader.setGravity(Gravity.CENTER);
			startHeader.setTextAppearance(this, R.style.TableHeader);
			startHeader.setText("Väljub");

			// Create TextView for table header
			TextView rideLength	= new TextView(this);
			rideLength.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
			rideLength.setTextSize(35);
			rideLength.setGravity(Gravity.CENTER);
			rideLength.setTextAppearance(this, R.style.TableHeader);
			rideLength.setText("Kestab");
			
			// Create TextView for table header
			TextView stopHeader	= new TextView(this);
			stopHeader.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
			stopHeader.setTextSize(35);
			stopHeader.setGravity(Gravity.CENTER);
			stopHeader.setTextAppearance(this, R.style.TableHeader);
			stopHeader.setText("Saabub");
			
			// Create TextView for table header
			TextView notifyHeader	= new TextView(this);
			notifyHeader.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
			notifyHeader.setText("");
			
			newRow.addView(startHeader);
			newRow.addView(rideLength);
			newRow.addView(stopHeader);
			newRow.addView(notifyHeader);
			
			tabel.addView(newRow);
			
			// Loop through timetable, skip headings
			for(int row = 1; row < timetable.length; row++) {
				// If the train skips this stop, we skip this row
				// Or if the train just does not go there, we skip this row
				if((timetable[row].length - 1) < finish_pointer || (timetable[row].length - 1) < start_pointer || timetable[row][finish_pointer] == "" || timetable[row][start_pointer] == "") continue;
				
				// Create a new line of LinearLayout
				newRow	= new LinearLayout(this);
				newRow.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				
				// Create TextView to show start time
				TextView startTime	= new TextView(this);
				startTime.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
				startTime.setTextSize(30);
				startTime.setGravity(Gravity.CENTER);
				startTime.setText(timetable[row][start_pointer]);
				
				// Create TextView to show stop time
				TextView stopTime	= new TextView(this);
				stopTime.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
				stopTime.setTextSize(30);
				stopTime.setOnClickListener(TripInformation.tripInformationListener);
				stopTime.setGravity(Gravity.CENTER);
				stopTime.setText(timetable[row][finish_pointer]);
				
				newRow.addView(startTime);

				// Calculate time between
				try {
					Date date1	= new SimpleDateFormat("HH:mm", Locale.ENGLISH).parse(timetable[row][start_pointer]);
					Date date2	= new SimpleDateFormat("HH:mm", Locale.ENGLISH).parse(timetable[row][finish_pointer]);
					
					long difference = date2.getTime() - date1.getTime(); 
					int days = (int) (difference / (1000 * 60 * 60 * 24));  
					int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60)); 
					int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);

					// Create TextView to length
					TextView timeDiff	= new TextView(this);
					timeDiff.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
					timeDiff.setTextSize(30);
					timeDiff.setGravity(Gravity.CENTER);
					timeDiff.setText(hours + ":" + min);
					
					// Add row to user view
					newRow.addView(timeDiff);
					
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				// Show notifications
				TextView userNotify	= new TextView(this);
				userNotify.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
				userNotify.setTextSize(20);
				userNotify.setGravity(Gravity.TOP);
				
				// Get timetable notifications
				String[] notifications	= TimeTables.user_notifications;
				
				if( notifications.length != 0 && notifications[row].length() != 0 && notifications[row] != "" ) {
					userNotify.setText(notifications[row]);
				}
				else {
					userNotify.setText("");
				}
				
				newRow.addView(stopTime);
				newRow.addView(userNotify);
				
				tabel.addView(newRow);
			}
		}
		else {
			// This is not a valid stop
			rowText.setText("Mine tagasi ja vali algus ja/või lõpppunkt uuesti.");
			showAlert(this, "Viga", "Valitud liinil puudub määratud algus ja/või lõpppunkt");
			
			tabel.addView(rowText);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Show pop-up dialog with error message
	 * 
	 * @param title Dialog title
	 * @param message Dialog message
	 */
	public static void showAlert(Context context, String title, String message) {
		// 1. Instantiate an AlertDialog.Builder with its constructor
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		// 2. Chain together various setter methods to set the dialog characteristics
		builder.setMessage(message)
		       .setTitle(title);

		// 3. Get the AlertDialog from create()
		AlertDialog dialog = builder.create();
		
		dialog.show();
	}
}
