package eu.niinemets.edelaraudtee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends Activity {
	public final static String USER_DIRECTION	= "com.example.firstapplication.DIRECTION";
	public final static String USER_START_POINT	= "com.example.firstapplication.STARTPOINT";
	public final static String USER_FINISH_POINT	= "com.example.firstapplication.FINISHPOINT";
	
	private OnItemSelectedListener mCorkyListener	= new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			changeSpinnerSource(view);
		}

		@Override
		public void onNothingSelected(AdapterView<?> view) {
			// TODO Auto-generated method stub
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Spinner direction 	= (Spinner) findViewById(R.id.direction);
		
		direction.setSelection(TimeTables.user_direction);
		direction.setOnItemSelectedListener(mCorkyListener);
		
		//changeSpinnerSource(findViewById(R.layout.activity_main));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	/**
	 * Called when the user clicks the Send button
	 * 
	 * @param view
	 */
	public void sendMessage(View view) {
		// Do something in response to the click
		Intent myIntent		= new Intent(this, DisplayMessageActivity.class);
		
		Spinner direction	= (Spinner) findViewById(R.id.direction);
		Spinner start_point	= (Spinner) findViewById(R.id.point_start);
		Spinner finish_point= (Spinner) findViewById(R.id.point_finish);
		
		if(start_point.getSelectedItemPosition() == finish_point.getSelectedItemPosition()) {
			DisplayMessageActivity.showAlert(this, "Hea valik", "Noh, algus- ja lõpppunkt võiksid siiski ju erineda.");
			
			return;
		}
		
		int line			= direction.getSelectedItemPosition();
		String start		= start_point.getSelectedItem().toString();
		String finish		= finish_point.getSelectedItem().toString();
		
		myIntent.putExtra(USER_DIRECTION, line);
		myIntent.putExtra(USER_START_POINT, start);
		myIntent.putExtra(USER_FINISH_POINT, finish);
		
		startActivity(myIntent);
	}
	
	/**
	 * Called when user selects a direction
	 * 
	 * @param view
	 */
	public void changeSpinnerSource(View view) {
		Spinner startPoints	= (Spinner) findViewById(R.id.point_start);
		Spinner stopPoints	= (Spinner) findViewById(R.id.point_finish);
		Spinner direction	= (Spinner) findViewById(R.id.direction);
		
		String start		= startPoints.getSelectedItem().toString();
		String finish		= stopPoints.getSelectedItem().toString();
		
		String[][] allPoints	= TimeTables.getTimetable(direction, start, finish);
		
		ArrayAdapter<String> stops		= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, allPoints[0]);
		stops.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		startPoints.setAdapter(stops);
		stopPoints.setAdapter(stops);
		
		startPoints.setSelection(TimeTables.user_start_point);
		stopPoints.setSelection(TimeTables.user_finish_point);
	}
	
	/**
	 * Show some information of the application or the author
	 */
	public void showAppInformation() {
		// Set title
		String title	= "Informatsioon";
		String message	= "";
		
		// Create a message
		message			+= "Autor: \t\t\t Risto Niinemets.\n\n";
		message			+= "E-mail:\t\t\t risto.niinemets@gmail.com\n";
		message			+= "Kodulehekülg:\t http://risto.niinemets.eu";
		
		// Show alert box
		DisplayMessageActivity.showAlert(this, title, message);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.menu_about:
				showAppInformation();
			case R.id.menu_settings:
				Intent myIntent = new Intent(this, SettingsActivity.class);
				startActivity(myIntent);
				return true;
		default:
				return super.onOptionsItemSelected(item);
		}
	}
}
