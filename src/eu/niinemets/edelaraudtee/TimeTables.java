package eu.niinemets.edelaraudtee;

import java.util.Arrays;
import java.util.List;

import android.widget.Spinner;

public class TimeTables {
	// Line 0 (Tallinn -> Tartu)
	public static String [][] tallinn_tartu 	= {
					{ "Tallinn", "Ülemiste", "Lagedi", "Aruküla", "Raasiku", "Kehra", "Aegviidu", "Nelijärve", "Jäneda", "Lehtse", "Tapa", "Tamsalu", "Kiltsi", "Rakke", "Vägeva", "Pedja", "Jõgeva", "Kaarepere", "Tabivere", "Kärkna", "Tartu" },
		/* 0210 */	{ "6:30", "6:39", "6:47", "6:55", "7:00", "7:09", "7:22", "7:26", "7:31", "7:39", "7:48", "8:04", "8:11", "8:19", "8:28", "8:37", "8:48", "8:58", "9:10", "9:19", "9:29" },
		/* 0010 */	{ "7:48", "7:58", "", "", "", "", "", "", "", "", "8:44", "8:57", "", "", "", "", "9:31", "", "", "", "10:02" },
		/* 0012 */	{ "14:08", "14:18", "", "", "", "", "", "", "", "", "15:04", "15:18", "", "", "", "", "15:51", "", "", "", "16:23" },
		/* 0322 */	{ "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "15:59", "16:09", "16:22", "16:31", "16:42" },
		/* 0212 */	{ "14:45", "14:55", "15:03", "15:11", "15:17", "15:26", "15:40", "15:44", "15:49", "15:57", "16:06", "16:20", "16:28", "16:37", "16:47", "16:56", "17:05", "17:16", "17:29", "17:37", "17:47" },
		/* 0014 */	{ "17:10", "17:20", "", "", "", "", "", "", "", "", "18:06", "18:20", "", "", "", "", "18:54", "", "", "", "19:25" },
		/* 0016 */	{ "20:10", "20:20", "", "", "", "", "", "", "", "", "21:06", "21:20", "", "", "", "", "21:52", "", "", "", "22:23" }
	};
	
	// Line 0 (Tartu -> Tallinn)
	public static String [][] tartu_tallinn 	= {
					{ "Tartu", "Kärkna", "Tabivere", "Kaarepere", "Jõgeva", "Pedja", "Vägeva", "Rakke", "Kiltsi", "Tamsalu", "Tapa", "Lehtse", "Jäneda", "Nelijärve", "Aegviidu", "Kehra", "Raasiku", "Aruküla", "Lagedi", "Ülemiste", "Tallinn" },
		/* 0011 */	{ "6:45", "", "", "", "7:17", "", "", "", "", "7:52", "8:06", "", "", "", "", "", "", "", "", "8:50", "8:59" },
		/* 0291 */	{ "7:46", "7:56", "8:04", "8:16", "8:25", "8:37", "8:45", "8:56", "9:07", "9:14", "9:27", "9:34", "9:40", "9:45", "9:49", "10:02", "10:11", "10:16", "10:26", "10:34", "10:43" },
		/* 0013 */	{ "14:10", "", "", "", "14:43", "", "", "", "", "15:18", "15:32", "", "", "", "", "", "", "", "", "16:16", "16:25" },
		/* 0321 */	{ "14:53", "15:03", "15:12", "15:25", "15:34" },
		/* 0015 */	{ "17:14", "", "", "", "17:45", "", "", "", "", "18:20", "18:34", "", "", "", "", "", "", "", "", "19:18", "19:27" },
		/* 0213 */	{ "18:14", "18:24", "18:33", "18:45", "18:54", "19:06", "19:16", "19:25", "19:35", "19:43", "19:57", "20:06", "20:12", "20:17", "20:21", "20:34", "20:43", "20:48", "20:57", "21:05", "21:14" },
		/* 0017 */	{ "20:13", "", "", "", "20:45", "", "", "", "", "21:20", "21:34", "", "", "", "", "", "", "", "", "22:18", "22:27" }
	};
	
	// Line 1 (Tallinn -> Narva)
	public static String [][] tallinn_narva 	= {
					{ "Tallinn", "Kitseküla", "Ülemiste", "Lagedi", "Aruküla", "Raasiku", "Kehra", "Aegviidu", "Nelijärve", "Jäneda", "Lehtse", "Tapa", "Kadrina", "Rakvere", "Kabala", "Sonda", "Kiviõli", "Püssi", "Kohtla", "Jõhvi", "Oru", "Vaivara", "Narva" },
		/* 0222 */	{ "15:40", "", "15:50", "", "", "16:04", "16:10", "16:19", "16:35", "", "16:43", "16:51", "17:00", "17:16", "17:27", "17:41", "17:51", "18:00", "18:07", "18:17", "18:27", "18:37", "18:46", "19:06" },
		/* 0224 */	{ "18:46", "", "18:56", "18:59", "19:06", "19:14", "19:21", "19:30", "19:44", "19:48", "19:53", "20:00", "20:09", "20:25", "20:35" }
	};
	
	// Line 1 (Narva -> Tallinn)
	public static String [][] narva_tallinn 	= {
					{ "Narva", "Vaivara", "Oru", "Jõhvi", "Kohtla", "Püssi", "Kiviõli", "Sonda", "Kabala", "Rakvere", "Kadrina", "Tapa", "Lehtse", "Jäneda", "Nelijärve", "Aegviidu", "Kehra", "Raasiku", "Aruküla", "Lagedi", "Ülemiste", "Kitseküla", "Tallinn" },
		/* 0221 */	{ "", "", "", "", "", "", "", "", "", "5:45", "5:57", "6:12", "6:20", "6:27", "6:31", "6:36", "6:49", "6:58", "7:04", "7:12", "7:17", "7:23", "7:29", "7:35" },
		/* 0223 */	{ "6:13", "6:35", "6:45", "6:54", "7:05", "7:14", "7:20", "7:28", "7:39", "7:55", "8:07", "8:24", "8:34", "8:40", "", "8:48", "9:01", "9:10", "9:15", "", "", "9:33", "", "9:42" }
	};
	
	// Line 2 (Tallinn -> Viljandi/Pärnu)
	public static String [][] tallinn_parnu	= { 
					{ "Tallinn", "Tallinn-Väike", "Liiva", "Männiku", "Saku", "Kasemetsa", "Kiisa", "Roobuka", "Vilivere", "Kohila", "Lohu", "Hagudi", "Rapla", "Keava", "Lelle", "Käru", "Kolu", "Türi", "Taikse", "Kärevere", "Ollepa", "Võhma", "Olustvere", "Sürgavere", "Viljandi", "Koogiste", "Eidapere", "Viluvere", "Tootsi", "Tori", "Pulli", "Pärnu Kaubajaam", "Pärnu" },
		/* 0231 */	{ "6:56", "7:05", "7:11", "7:17", "7:23", "7:27", "7:34", "7:37", "7:41", "7:47", "7:54", "8:00", "8:09", "8:16", "8:30", "", "", "", "", "", "", "", "", "", "", "8:38", "8:45", "8:59", "9:10", "9:16", "9:28", "9:35", "9:42" },
		/* 0041 */	{ "7:52", "8:01", "8:06", "", "8:15", "", "8:23", "", "", "8:31", "", "", "8:48", "", "9:03", "9:11", "", "9:24", "9:29", "9:34", "9:40", "9:46", "9:54", "10:01", "10:13" },
		/* 0233 */	{ "8:40", "8:49", "8:56", "9:02", "9:09", "9:14", "9:22", "9:25", "9:29", "9:35", "9:41", "9:47", "9:57" },
		/* 0235 */	{ "10:52", "11:01", "11:07", "11:13", "11:19", "11:23", "11:30", "11:33", "11:37", "11:43", "11:50", "11:56", "12:05" },
		/* 0043 */	{ "13:30", "13:39", "13:45", "", "13:54", "", "14:02", "", "", "14:10", "", "", "14:27", "", "14:41", "14:48", "", "15:07", "15:12", "15:18", "15:25", "15:31", "15:41", "15:51", "16:06" },
		/* 0243 */	{ "14:23", "14:32", "14:38", "14:44", "14:49", "14:54", "15:00", "15:03", "15:07", "15:14", "15:21", "15:27", "15:36", "15:43", "15:54" },
		/* 0045 */	{ "16:31", "16:40", "16:45", "", "16:54", "", "17:03", "", "", "17:12", "", "", "17:29", "", "17:46", "17:53", "", "18:13", "18:18", "18:24", "18:31", "18:37", "18:47", "18:57", "19:12" },		
		/* 0237 */	{ "17:29", "17:38", "17:44", "17:50", "17:55", "18:00", "18:06", "18:09", "18:13", "18:20", "18:27", "18:33", "18:43", "18:50", "19:01", "", "", "", "", "", "", "", "", "", "", "19:09", "19:09", "19:16", "19:30", "19:41", "19:47", "19:59", "20:06", "20:13" },
		/* 0261 */	{ "18:42", "18:51", "18:57", "19:03", "19:09", "19:13", "19:20", "19:23", "19:27", "19:33", "19:40", "19:46", "19:55" },
		/* 0371 */	{ "19:40", "19:49", "19:55", "20:01", "20:07", "20:11", "20:18", "20:21", "20:25", "20:32", "20:39", "20:45", "20:54", "21:01", "21:12", "21:19", "21:30", "21:40" },
		/* 0373 */	{ "21:54", "22:03", "22:09", "22:15", "22:21", "22:25", "22:32", "22:35", "22:39", "22:45", "22:52", "22:58", "23:07", "23:14", "23:25", "23:32", "23:43", "23:53" }
	};
	
	// Line 2 (Viljandi/Pärnu -> Tallinn)
	public static String [][] parnu_tallinn	= {
					{ "Pärnu", "Pärnu Kaubajaam", "Pulli", "Tori", "Tootsi", "Viluvere", "Eidapere", "Koogiste", "Viljandi", "Sürgavere", "Olustvere", "Võhma", "Ollepa", "Kärevere", "Taikse", "Türi", "Kolu", "Käru", "Lelle", "Keava", "Rapla", "Hagudi", "Lohu", "Kohila", "Vilivere", "Roobuka", "Kiisa", "Kasemetsa", "Saku", "Männiku", "Liiva", "Tallinn-Väike", "Tallinn" },
		/* 0372 */ 	{ "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "5:19", "5:28", "5:35", "5:45", "5:54", "6:03", "6:11", "6:17", "6:25", "6:29", "6:33", "6:37", "6:42", "6:46", "6:52", "6:59", "7:05", "7:13" },
		/* 0374 */	{ "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "6:16", "6:25", "6:32", "6:42", "6:51", "7:00", "7:08", "7:14", "7:22", "7:26", "7:30", "7:34", "7:39", "7:43", "7:49", "7:56", "8:01", "8:09" },
		/* 0042 */	{ "", "", "", "", "", "", "", "", "6:43", "6:53", "6:59", "7:09", "7:14", "7:20", "7:26", "7:32", "", "7:44", "7:55", "", "8:11", "", "", "8:31", "", "", "8:39", "", "8:46", "", "8:56", "9:01", "9:09" },
		/* 0230 */ 	{ "7:14", "7:22", "7:28", "7:40", "7:47", "7:56", "8:11", "8:17", "", "", "", "", "", "", "", "", "", "", "8:27", "8:36", "8:45", "8:53", "8:59", "9:07", "9:11", "9:15", "9:19", "9:24", "9:28", "9:34", "9:41", "9:47", "9:55" },
		/* 0376 */	{ "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "9:34", "", "9:48", "9:58", "", "10:13", "", "", "10:30", "", "", "10:38", "", "10:45", "", "10:55", "11:01", "11:09" },
		/* 0234 */	{ "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "10:55", "11:04", "11:10", "11:18", "11:22", "11:26", "11:30", "11:35", "11:39", "11:45", "11:52", "11:58", "12:06" },
		/* 0236 */	{ "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "13:27", "13:36", "13:42", "13:50", "13:54", "13:58", "14:02", "14:07", "14:11", "14:17", "14:26", "14:34", "14:42" },
		/* 0044 */	{ "", "", "", "", "", "", "", "", "13:26", "13:36", "13:42", "13:52", "13:57", "14:03", "14:09", "14:15", "", "14:27", "14:41", "", "14:56", "", "", "15:13", "", "", "15:21", "", "15:28", "", "15:38", "15:44", "15:52" },
		/* 0244 */	{ "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "16:30", "16:40", "16:49", "16:57", "17:03", "17:11", "17:15", "17:19", "17:23", "17:28", "17:32", "17:38", "17:44", "17:50", "17:58" },
		/* 0046 */	{ "", "", "", "", "", "", "", "", "16:32", "16:44", "16:50", "16:59", "17:04", "17:09", "17:14", "17:20", "", "17:33", "17:46", "", "18:02", "", "", "18:19", "", "", "18:27", "", "18:34", "", "18:45", "18:51", "18:59" },
		/* 0238 */	{ "17:11", "17:19", "17:25", "17:37", "17:44", "17:53", "18:08", "18:14", "", "", "", "", "", "", "", "", "", "", "18:24", "18:33", "18:43", "18:51", "18:57", "19:06", "19:10", "19:14", "19:20", "19:25", "19:29", "19:35", "19:43", "19:49", "19:57" },
		/* 0262 */	{ "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "20:08", "20:17", "20:23", "20:31", "20:35", "20:39", "20:43", "20:48", "20:52", "20:58", "21:05", "21:11", "21:19" }
	};
	
	// Line 3 (Tartu -> Valga)
	public static String [][] tartu_valga		= {
					{ "Tartu", "Aardla", "Ropka", "Nõo", "Tõravere", "Peedu", "Elva", "Palupera", "Puka", "Mägiste", "Keeni", "Sangaste", "Valga" },
		/* 0210 */	{ "9:39", "9:43", "9:51", "9:59", "10:05", "10:09", "10:14", "10:25", "10:35", "10:40", "10:48", "10:58", "11:09" },
		/* 0212 */	{ "18:16", "18:20", "18:28", "18:36", "18:42", "18:46", "18:50", "19:02", "19:11", "19:17", "19:25", "19:34", "19:46" }
	};
	
	// Line 3 (Valga -> Tartu)
	public static String [][] valga_tartu		= {
					{ "Valga", "Sangaste", "Keeni", "Mägiste", "Puka", "Palupera", "Elva", "Peedu", "Tõravere", "Nõo", "Ropka", "Aardla", "Tartu" },
		/* 0281 */	{ "6:00", "6:12", "6:21", "6:29", "6:35", "6:45", "6:56", "7:00", "7:04", "7:10", "7:18", "7:26", "7:30" },
		/* 0213 */	{ "16:42", "16:54", "17:04", "17:12", "17:17", "17:27", "17:37", "17:41", "17:46", "17:52", "18:00", "18:08", "18:12" }
	};
	
	// Line 4 (Tartu -> Koidula)
	public static String[][] tartu_koidula		= {
					{ "Tartu", "Kirsi", "Ülenurme", "Uhti", "Reola", "Vana-Kuuste", "Rebase", "Vastse-Kuuste", "Valgemetsa", "Kiidjärve", "Taevaskoja", "Põlva", "Holvandi", "Ruusa", "Veriora", "Ilumetsa", "Orava", "Koidula" },
		/* 0380 */	{ "10:14", "10:18", "10:24", "10:27", "10:33", "10:36", "10:40", "10:49", "10:53", "10:57", "11:01", "11:09", "11:15", "11:20", "11:27", "11:32", "11:39", "11:51" },
		/* 0382 */	{ "17:57", "18:01", "18:07", "18:10", "18:16", "18:19", "18:23", "18:32", "18:36", "18:40", "18:44", "18:52", "18:58", "19:03", "19:10", "19:15", "19:22", "19:34" }
	};
	
	// Line 4 (Koidula -> Tartu)
	public static String[][] koidula_tartu		= {
					{ "Koidula", "Orava", "Ilumetsa", "Veriora", "Ruusa", "Holvandi", "Põlva", "Taevaskoja", "Kiidjärve", "Valgemetsa", "Vastse-Kuuste", "Rebase", "Vana-Kuuste", "Reola", "Uhti", "Ülenurme", "Kirsi", "Tartu" },
		/* 0291 */	{ "5:46", "5:59", "6:05", "6:12", "6:18", "6:23", "6:31", "6:36", "6:40", "6:44", "6:50", "6:58", "7:01", "7:07", "7:11", "7:15", "7:22", "7:26" },
		/* 0383 */	{ "15:24", "15:37", "15:43", "15:50", "15:56", "16:01", "16:09", "16:14", "16:18", "16:22", "16:28", "16:36", "16:39", "16:45", "16:49", "16:53", "17:00", "17:04" }
	};
	
	// Notifications
	public static String [] tallinn_tartu_notify	= { "", "Valgasse", "E-R", "R, P", "E-R", "Valka", "", "R, P" };
	public static String [] tartu_tallinn_notify	= { "", "E-L", "Koidulast", "R, P", "E-R", "E-R, P", "Valgast", "R-P" };
	public static String [] tallinn_narva_notify	= { };
	public static String [] narva_tallinn_notify	= { };
	public static String [] tallinn_parnu_notify	= { "", "", "", "", "", "RLP", "", "", "", "", "", "" };
	public static String [] parnu_tallinn_notify	= { "", "E-R", "", "", "", "L, P", "", "", "RLP", "", "", "", "" };
	public static String [] tartu_valga_notify		= { "", "Tallinnast", "Tallinnast" };
	public static String [] valga_tartu_notify		= { "", "", "Tallinnasse" };
	public static String [] tartu_koidula_notify	= { "", "L, P", "E-R, P" };
	public static String [] koidula_tartu_notify	= { "", "Tln E-L", "L, P" };
	
	/* Accessible starting and finishing points, which will be set
	 * after user calls getTimeTable method
	 */
	public static int user_direction	= 2;
	public static int user_start_point	= 0;
	public static int user_finish_point	= 0;
	public static String [] user_notifications = tallinn_parnu_notify;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Get the timetable according to starting and finishing point
	 * and also according to direction. If the finishing point is before
	 * starting point, timetable will be reversed.
	 * 
	 * @param direction
	 * @param start_point
	 * @param finish_point
	 * 
	 * @return String[][] The wanted timetable
	 */
	public static String[][] getTimetable(int direction, String start_point, String finish_point) {
		String [][] timetable 	= tallinn_parnu;
		String [] notifications	= tallinn_parnu_notify;
		
		// Select the right timetable
		if (direction == 0) {
			// Tallinn - Tartu - Tallinn
			timetable 		= tallinn_tartu;
			notifications	= tallinn_tartu_notify;
		}
		else if (direction == 1) {
			// Tallinn - Rakvere - Narva - Rakvere - Tallinn
			timetable 		= tallinn_narva;
			notifications	= tallinn_narva_notify;
		}
		else if(direction == 2) {
			// Tallinn - Lelle - Viljandi/Pärnu - Lelle - Tallinn
			timetable 		= tallinn_parnu;
			notifications	= tallinn_parnu_notify;
		}
		else if(direction == 3) {
			// Tartu - Valga - Tartu
			timetable 		= tartu_valga;
			notifications	= tartu_valga_notify;
		}
		else if(direction == 4) {
			// Tartu - Põlva - Koidula
			timetable		= tartu_koidula;
			notifications	= tartu_koidula_notify;
		}
		else {}
		
		// Search position index of starting and finishing points
		List<String> list	= Arrays.asList(timetable[0]);
		int start_pointer	= list.indexOf(start_point);
		int finish_pointer	= list.indexOf(finish_point);
		
		// If starting point is actually after finishing point, then change the direction of timetable
		if(start_pointer > finish_pointer) {
			// Select the right timetable AGAIN
			if (direction == 0) {
				// Tallinn - Tartu - Tallinn
				timetable 		= tartu_tallinn;
				notifications	= tartu_tallinn_notify;
			}
			else if (direction == 1) {
				// Tallinn - Rakvere - Narva - Rakvere - Tallinn
				timetable 		= narva_tallinn;
				notifications	= narva_tallinn_notify;
			}
			else if(direction == 2) {
				// Tallinn - Lelle - Viljandi/Pärnu - Lelle - Tallinn
				timetable 		= parnu_tallinn;
				notifications	= parnu_tallinn_notify;
			}
			else if(direction == 3) {
				// Tartu - Valga - Tartu
				timetable 		= valga_tartu;
				notifications	= valga_tartu_notify;
			}
			else if(direction == 4) {
				// Tartu - Põlva - Koidula
				timetable 		= koidula_tartu;
				notifications	= koidula_tartu_notify;
			}
			
			// Research position index
			list			= Arrays.asList(timetable[0]);
			start_pointer	= list.indexOf(start_point);
			finish_pointer	= list.indexOf(finish_point);
		}
		
		user_start_point	= start_pointer;
		user_finish_point	= finish_pointer;
		user_direction		= direction;
		user_notifications	= notifications;
		
		return timetable;
	}
	
	/**
	 * 
	 * @see getTimeTable(int direction, String start_point, String finish_point)
	 * @param line
	 * @param start_point
	 * @param finish_point
	 * @return The wanted timetable
	 */
	public static String[][] getTimetable(Spinner line, String start_point, String finish_point) {
		int direction 			= line.getSelectedItemPosition();
		
		return getTimetable(direction, start_point, finish_point);
	}
	
	/**
	 * Get user notifications that has been set when we fetched timetable
	 * 
	 * @return User notifications
	 */
	public static String[] getNotifications() {
		return user_notifications;
	}
}
